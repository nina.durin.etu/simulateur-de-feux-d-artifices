# Simulation de feux d'artifices

![Capture d'écran du programme](./Capture_d_écran.png)  

## Introduction

Ce petit programme simple simulant des explosions de feux d'artifices montre comment, avec quelques notions mathématiques, on peut reproduire ce merveilleux spectacle.

## Fonctionnalités

- Un show coloré et aléatoire de feux d'artifices en plein écran

## Cas d'utilisation

Un des cas d'utilisation est en tant qu'écran de veille (ou screen saver). Il est parfait pour cela car il est aléatoire et très reposant à regarder. Il peut aussi servir dans un but éducatif, comme exemple d'utilisation de la bibliothèque Pygame pour Python, car il est écrit avec. 

## Prérequis

 - Python
 - Module Pygame pour python (`pip install pygame`)

## Installation

`pip install pygame`  
`git clone https://gitlab.univ-lille.fr/nina.durin.etu/simulateur-de-feux-d-artifices.git`  

## Démarrage rapide

`cd simulateur-de-feux-d-artifices`  
`python3 main.py`  

## Licence

`CC-0`  
Ce projet est sous licence libre CC-0, il peut donc être librement partagé, modifié, utilisé commercialement sans aucune attribution.
