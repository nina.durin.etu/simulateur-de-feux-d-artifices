import pygame
from pygame.locals import *
import random
import math

pygame.init()
screen_info = pygame.display.Info()
window_size = (screen_info.current_w, screen_info.current_h)
screen = pygame.display.set_mode(window_size, FULLSCREEN)
pygame.display.set_caption("Fireworks Simulation")

frame_rate = 60
clock = pygame.time.Clock()
screen.fill((0, 0, 0))
num_fireworks = 10
num_particles = 500

class Firework():
    """docstring for Firework."""

    def __init__(self):
        self.x = random.randrange(10,window_size[0])
        self.y = 720
        self.vy = -6 + random.random()*3
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.radius = random.randrange(2,6)
        self.exploded = False
        self.exploding = False
        self.explode = 0
        self.maxexplode = 120

    def update(self):
        self.y += self.vy
        self.vy += 0.03
        if self.exploding:
            self.explode += 1
            self.vy = 0
        if -1 < self.vy < 0.1:
            self.exploded = True
            self.exploding = True
        if self.explode == self.maxexplode:
            self.explode = 0
            self.exploding = False



class Particle():
    """docstring for Particle."""

    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        angle = random.random()*math.pi*2
        force = random.random() * 2
        self.vx = math.cos(angle) * force
        self.vy = math.sin(angle) * force - 0.5
        self.color = color

    def update(self):
        self.x += self.vx
        self.y += self.vy
        self.vy += 0.02

fireworks = []
for i in range(num_fireworks):
  fireworks.append(Firework())

particles = []
for i in range(num_fireworks):
  particles.append([])
  for j in range(num_particles):
    particles[i].append(None)

run = True
while run:

  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      run = False
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
            run = False

  screen.fill((0, 0, 0))

  for i in range(num_fireworks):
    fireworks[i].update()
    pygame.draw.circle(screen, fireworks[i].color, (int(fireworks[i].x), int(fireworks[i].y)), fireworks[i].radius)

    for j in range(num_particles):
      if particles[i][j] != None:
        particles[i][j].update()
        pygame.draw.circle(screen, particles[i][j].color, (int(particles[i][j].x), int(particles[i][j].y)), 1)

    if ((fireworks[i].exploded) and (not fireworks[i].exploding)):
      fireworks[i] = Firework()
      for j in range(num_particles):
        particles[i][j] = None
    if fireworks[i].exploding and fireworks[i].explode == 0:
      for j in range(num_particles):
        particles[i][j] = Particle(fireworks[i].x, fireworks[i].y, fireworks[i].color)


  pygame.display.flip()


  clock.tick(frame_rate)
pygame.quit()
